import {createRoot} from 'react-dom/client'
import App from './App'
import './index.css'
import {insertCoin} from 'playroomkit'

const root = document.getElementById('root')
insertCoin({
  skipLobby: true // 自定义大厅
}).then(() => (
  createRoot(root).render( <App />)
))

