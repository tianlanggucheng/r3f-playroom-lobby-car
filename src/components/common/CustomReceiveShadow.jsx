import {CameraControls} from '@react-three/drei'
import {useEffect, useRef} from 'react'

export const CustomReceiveShadow = (props) => {
  const {scene, onCustomMaterial, castShadow} = props
  useEffect(() => {
    scene.traverse(child => {
      if (child.isMesh) {
        if (castShadow) {
          child.castShadow = true
          child.receiveShadow = true
        }
        onCustomMaterial && onCustomMaterial(child)
      }
    })
  }, [scene])
  return null
}
