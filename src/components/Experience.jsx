import {Lobby} from './lobby/Lobby.jsx'
import {useMultiplayerState} from 'playroomkit'
import {__GAME_STATE_KEY__, GAME_STATES} from './UI.jsx'
import {Game} from './game/Game.jsx'

export const Experience = () => {

  const [gameState] = useMultiplayerState(__GAME_STATE_KEY__, GAME_STATES.LOBBY)
  console.log(gameState)
  return (
    <>
      {gameState === GAME_STATES.LOBBY && <Lobby/>}
      {gameState === GAME_STATES.GAME && <Game/>}
    </>
  )
}
