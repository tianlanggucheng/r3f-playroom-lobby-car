import {Environment, Lightformer} from '@react-three/drei'

export const CustomGameLight = () => {
  return (
    <>
      <ambientLight intensity={0.4}/>
      <Environment>
        <Lightformer
          position={[5, 5, 5]}
          form="rect" // circle | ring | rect (optional, default = rect)
          intensity={1} // power level (optional = 1)
          color="white" // (optional = white)
          scale={[10, 10]} // Scale it any way you prefer (optional = [1, 1])
          target={[0, 0, 0]} // Target position (optional = undefined)
        />
      </Environment>
      <pointLight position={[0, 5, 0]} intensity={2.5} distance={10}/>
      <pointLight
        position={[5, 5, 0]}
        intensity={10.5}
        distance={10}
        color="pink"
      />
      <pointLight
        position={[-5, 5, 0]}
        intensity={10.5}
        distance={15}
        color="blue"
      />
      <directionalLight position={[10, 10, 10]} intensity={0.4}/>
    </>
  )
}
