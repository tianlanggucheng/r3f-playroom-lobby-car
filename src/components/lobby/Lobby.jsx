import {useGLTF} from '@react-three/drei'
import {CustomLight} from './CustomLight.jsx'
import {myPlayer, usePlayersList} from 'playroomkit'
import {Players} from './Players.jsx'
import {CustomCameraControls} from '../common/CustomCameraControls.jsx'
import {CustomReceiveShadow} from '../common/CustomReceiveShadow.jsx'

export const Lobby = (props) => {
  const me = myPlayer()
  // 每次有新玩家加入或更新数据时，我们都可以添加true刷新玩家列表
  const players = usePlayersList(true)
  const {scene} = useGLTF('/models/garage.glb')

  return (
    <>
      <CustomCameraControls
        pos={[4.2, 2, 7.5]}
        tatget={[0, 0.15, 0]}
      />
      <CustomReceiveShadow scene={scene} castShadow/>
      <CustomLight>
        <primitive object={scene}/>
        <Players players={players} user={me?.id}/>
      </CustomLight>
    </>
  )
}

useGLTF.preload('/models/garage.glb')
