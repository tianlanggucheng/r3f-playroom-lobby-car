import {CARS} from './Players.jsx'
import {Clone, useGLTF} from '@react-three/drei'
import {degToRad} from 'three/src/math/MathUtils.js'
import {CustomReceiveShadow} from '../common/CustomReceiveShadow.jsx'
import {MeshStandardMaterial} from 'three'

export const Car = (props) => {
  const {model = CARS[0]} = props
  const  {scene} = useGLTF(`/models/cars/${model}.glb`)

  const onCustomMaterial = (child) => {
    if (child.material.name === "window") {
      child.material.transparent = true;
      child.material.opacity = 0.5;
    }
    if (
      child.material.name.startsWith("paint") ||
      child.material.name === "wheelInside"
    ) {
      // child.material.rougness = 0.1;
      // child.material.metalness = 1.0;
      child.material = new MeshStandardMaterial({
        color: child.material.color,
        metalness: 0.5,
        roughness: 0.1,
      });
    }
    if (child.material.name.startsWith("light")) {
      child.material.emissive = child.material.color;
      child.material.emissiveIntensity = 4;
      child.material.toneMapped = false;
    }
  }

  return (
    <>
      <CustomReceiveShadow scene={scene} onCustomMaterial={onCustomMaterial}/>
      <group {...props}>
        <Clone object={scene} rotation-y={degToRad(180)} castShadow/>
      </group>
    </>
  )
}
