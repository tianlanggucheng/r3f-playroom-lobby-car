import { useGLTF } from '@react-three/drei'
import {CarBillboard} from './CarBillboard.jsx'

const CAR_SPACING = 2.5

export const CARS = [
  'sedanSports',
  'raceFuture',
  'taxi',
  'ambulance',
  'police',
  'truck',
  'firetruck'
]

export const Players = ({players, user, ...props}) => {

  return (
    players.map((player, idx) => (
      <group key={player.id} position={[
        idx * CAR_SPACING - ((players.length - 1) * CAR_SPACING) / 2
        , 0, 0]}>
        <CarBillboard player={player} user={user}/>
      </group>
    ))
  )
}

CARS.forEach(model => useGLTF.preload(`/models/cars/${model}.glb`))
