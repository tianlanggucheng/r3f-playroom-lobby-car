import {Canvas} from '@react-three/fiber'
import {Experience} from './components/Experience'
// import {Bloom, EffectComposer} from '@react-three/postprocessing'
// import { BlurPass, Resizer, KernelSize, Resolution } from 'postprocessing'
import {Leva} from 'leva'
import {UI} from './components/UI.jsx'

function App() {
  return (
    <>
      <Leva hidden />
      <Canvas
        shadows
        camera={{position: [4.2, 1.5, 7.5], fov: 45, near: 0.5}}
      >
        <color attach="background" args={['#333']}/>
        <Experience/>
        {/* <EffectComposer>
          <Bloom luminanceThreshold={1} intensity={1.22}/>
        </EffectComposer> */}
      </Canvas>
      <UI />
    </>
  )
}

export default App
